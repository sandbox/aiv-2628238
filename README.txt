Implementation of https://www.drupal.org/node/233997#comment-10632620.

It was developed keeping the codebase of Download Count module in mind
(variable names, etc.).

The reason of development of one more download counter is that none of
the existing solutions work for me. Just-mentioned Download Count module
works only with 'managed' files (those which were added to 'file_managed'
DB table).

My module works both with managed and unmanaged files. It counts the
download just before the actual transfer starts. To be more exact,
it is triggered when URI like '/system/files/...' is requested by implementing
hook_file_download(). We also take care of our implementation to be triggered
in last turn in the loop with all triggered implementations of this hook.

Our hook_file_download() implementation does not check access rights
(it may check permissions for logging purposes, though). It actually does
only one thing: log who and when downloaded such or such file.


@author Alexander Ishmuradov <ishmuradov@gmail.com>, http://ishmuradov.com

